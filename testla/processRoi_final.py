import numpy as np
import cv2
import math

def cart2pol(x, y):
  r = np.sqrt(x**2 + y**2)
  ang = np.arctan2(y, x)
  return(r, ang)

def deg2rad(ang):
  return constrainRadians(ang*np.pi/180.0)

def rad2deg(ang):
  return constrainRadians(ang)*180.0/np.pi

def isWithin(value, value2, within):
  return ((value + within) >= value2
          and (value2 + within) >= value)

def constrainRadians(ang):
  while not isWithin(ang, np.pi, np.pi):
    if (ang < 0):
      ang += 2*np.pi
    elif (ang > 2*np.pi):
      ang -= 2*np.pi
  return ang

##
# Inputs
#   aRoi - input image (np matrix)
#   aLineSize - minimum size (in pixels) of target
#   aDegreesFromLinear - angle offset from horizontal or vertical to ignore
#   aDebug - debug windows
# Output
#   Array of np rectangles
##
def processRoi(aRoi, aLineSize, aDegreesFromLinear, aDebug=False):
  # Improve image contrast
  eqRoi = cv2.equalizeHist(aRoi)

  # Morphological operator to remove noise and define edges (after emperical testing)
  eqRoi = cv2.dilate(eqRoi, np.ones((3,3), np.uint8))
  eqRoi = cv2.erode(eqRoi, np.ones((5,5), np.uint8))

  # Remove noise and preserve edges with bilateral filter
  # Invert results (so that targets are larger values)
  filtered = 255-cv2.bilateralFilter(eqRoi, -1, 100, 5)
  # Gamma correction (excentuates features) gamma=2
  filtered = np.uint8(np.power(np.float32(filtered)/255.0, 2.0)*255)
  # Find edges
  allEdges = cv2.Laplacian(filtered,cv2.CV_8U)
  # Detect features from edges
  val = (np.max(allEdges) - np.min(allEdges))/2
  ret, edges = cv2.threshold(allEdges,val,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
  edges = cv2.GaussianBlur(edges, (5,5), 2)
  ret, edges = cv2.threshold(edges,0,255,cv2.THRESH_BINARY)

  # Detect lines based on size
  # lines = cv2.HoughLinesP(edges,1,np.pi/180, 100, aLineSize, 5)
  # im2, contours, hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  # cv2.drawContours(aRoi, contours, -1, (0,255,0), 3)
  d = cv2.createLineSegmentDetector(cv2.LSD_REFINE_ADV)
  alllines, width, prec, nfa	=	d.detect(edges)
  lines = []
  for line in alllines:
    x = line[0][2]-line[0][0]
    y = line[0][3]-line[0][1]
    r,ang = cart2pol(x,y)
    if (r >= aLineSize
        and (not isWithin(rad2deg(ang), 0, 2*aDegreesFromLinear))
        and (not isWithin(rad2deg(ang), 90, 2*aDegreesFromLinear))
        and (not isWithin(rad2deg(ang), 180, 2*aDegreesFromLinear))
        and (not isWithin(rad2deg(ang), 270, 2*aDegreesFromLinear))
        and (not isWithin(rad2deg(ang), 360, 2*aDegreesFromLinear))):
          lines.append(line[0])
          if aDebug:
            print(rad2deg(ang))

  if (aDebug):
    print(lines)
    cv2.namedWindow('filtered', cv2.WINDOW_NORMAL)
    cv2.namedWindow('orig', cv2.WINDOW_NORMAL)
    cv2.namedWindow('result', cv2.WINDOW_NORMAL)
    cv2.imshow('orig',aRoi)
    cv2.imshow('filtered',filtered)
    cv2.imshow('result',edges)
    cv2.waitKey()
  return lines
