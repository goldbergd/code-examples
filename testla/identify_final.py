import numpy as np
import cv2
import math
from processRoi_final import processRoi

k_debug = False
# known Parameters
k_cellAreaInMm2 = 156.0
k_minLineLengthMm = 2.0
k_degreesFromLinear = 5

#load image
img = cv2.imread('EL_20171221163003_OK.bmp',0)

# Sharpen with Unsharp mask
blur = cv2.GaussianBlur(img, (9,9), 4)
sharpenedImg = cv2.addWeighted(img, 1.5, blur, -0.5, 0, img)

##
# Find areas of interest
##

# Use OTSU to get initial estimate
threshValue, binIm = cv2.threshold(sharpenedImg,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

# Morphological operators to remove noise and define edges
binIm = cv2.morphologyEx(binIm, cv2.MORPH_CLOSE,np.ones((20,20),np.uint8))
binIm = cv2.morphologyEx(binIm, cv2.MORPH_OPEN,np.ones((20,20),np.uint8))

# Be conservative about accepting region as region of interest
binIm = cv2.erode(binIm, np.ones((20,20),np.uint8))

# Find connected components
numLabels, labels, stats, centroids = cv2.connectedComponentsWithStats(binIm, 8, cv2.CV_32S)

# Purely for debug
binImShow = np.reshape(binIm,(binIm.shape[0],binIm.shape[1],1))
binImShow = np.uint8(np.concatenate((binImShow,binImShow,binImShow),2))

avgSz = 0.0
count = 0
for i in range(numLabels):
  # Kept finding ROIs that were larger than reasonable. Filter out quickly
  if (stats[i][cv2.CC_STAT_AREA] < binIm.size/8):
    # Draw circle at ROI
    cv2.circle(binImShow, (int(centroids[i][0]),int(centroids[i][1])), 20, (0,255,0),-1)
    avgSz += float(stats[i][cv2.CC_STAT_AREA])
    count += 1

avgSz /= count
pixPerMm = math.sqrt(avgSz) / math.sqrt(k_cellAreaInMm2)
if k_debug:
  cv2.namedWindow('bin', cv2.WINDOW_NORMAL)
  cv2.imshow('bin',binImShow)
  print("Average area pix:")
  print(avgSz)
  print("Pix per mm:")
  print(pixPerMm)

##
# Go through each identified target, and find defects
##
defects = []
for i in range(numLabels):
  # Kept finding ROIs that were larger than reasonable. Filter out quickly
  if (stats[i][cv2.CC_STAT_AREA] < binIm.size/8):
    x = stats[i][cv2.CC_STAT_LEFT]
    y = stats[i][cv2.CC_STAT_TOP]
    h = stats[i][cv2.CC_STAT_HEIGHT]
    w = stats[i][cv2.CC_STAT_WIDTH]
    roi = sharpenedImg[y:y+h, x:x+w]
    output = processRoi(roi, k_minLineLengthMm * pixPerMm,k_degreesFromLinear,k_debug)
    if output:
      for point in output:
        defects.append((point[0]+x, point[1]+y, point[2]+x, point[3]+y))

imgOut = np.reshape(img,(img.shape[0],img.shape[1],1))
imgOut = np.uint8(np.concatenate((imgOut,imgOut,imgOut),2))

for defect in defects:
  # TODO: draw ellipse like example
  # cv2.ellipse(binImShow, (int(centroids[i][0]),int(centroids[i][1])), (int(40),int(10)), 45.0, 0.0, 360.0, (255, 0, 0), 2)
  # Draw cyan rectangle
  cv2.rectangle(imgOut, (int(defect[0]), int(defect[1])), (int(defect[2]),int(defect[3])), (255,255,0), 2)

cv2.imwrite('goldberg_output.bmp', imgOut)
if k_debug:
  cv2.namedWindow('output', cv2.WINDOW_NORMAL)
  cv2.imshow('output')
  cv2.waitKey()
