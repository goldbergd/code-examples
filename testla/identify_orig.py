import numpy as np
import cv2
import math

def processRoi(roi, lineSize):
  # a = hog(roi)
  # print(a)
  # cv2.imshow('sum', allRois)
  # normRoi = np.uint8(roi - allRois)
  # filtered = cv2.boxFilter(roi,-1,(3,3))
  # window = cv2.createHanningWindow((roi.shape[1],roi.shape[0]), 5)*1/2
  # roiWind = np.uint8(255-roi * window)
  # print(cv2.createHanningWindow((5,5), 5))
  # filtered = cv2.bilateralFilter(roi, 7, 5, 7)
  eqRoi = cv2.equalizeHist(roi)
  eqRoi = cv2.dilate(eqRoi, np.ones((3,3), np.uint8))
  eqRoi = cv2.erode(eqRoi, np.ones((5,5), np.uint8))
  filtered = 255-cv2.bilateralFilter(eqRoi, -1, 100, 5)
  # filtered = 255-cv2.erode(filtered, np.ones((9,9), np.uint8))
  # ret, edgesPossible = cv2.threshold(filtered,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
  # edgesPossible = edgesPossible/255
  # newCandidates = np.uint8(edgesPossible*cv2.equalizeHist(roi))
  # ret, edges = cv2.threshold(newCandidates,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
  # thresh = cv2.adaptiveThreshold(eqRoi,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
  #           cv2.THRESH_BINARY,11,2)
  # orb = cv2.ORB_create()
  # kp = orb.detect(eqRoi,None)
  # kp, des = orb.compute(eqRoi, kp)
  # img=cv2.drawKeypoints(eqRoi, kp, None, color=(0,255,0), flags=0)

  # eqRoi = cv2.erode(eqRoi,cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5)),iterations=1)
  # eqRoi = cv2.morphologyEx(eqRoi, cv2.MORPH_CLOSE,cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3)))
  # out,roiBin = cv2.threshold(eqRoi,127,255,cv2.THRESH_BINARY)
  # roiBin = cv2.morphologyEx(eqRoi, cv2.MORPH_CLOSE,cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5)))
  # out,roiBin = cv2.threshold(roiBin,127,255,cv2.THRESH_BINARY)
  cv2.namedWindow('test', cv2.WINDOW_NORMAL)
  cv2.namedWindow('orig', cv2.WINDOW_NORMAL)
  cv2.imshow('orig',cv2.equalizeHist(roi))
  cv2.imshow('test',filtered)
  # edges = cv2.Canny(filtered, 50,10)
  allEdges = cv2.Laplacian(filtered,cv2.CV_8U)
  val = (np.max(allEdges) - np.min(allEdges))/4
  # ret, edges = cv2.threshold(allEdges,val,255,cv2.THRESH_BINARY)
  ret, edges = cv2.threshold(allEdges,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
  # d = cv2.createLineSegmentDetector()
  # lines, width, prec, nfa = d.detect(filtered)
  # d = cv2.SimpleBlobDetector_create()
  # keys = d.detect(eqRoi)
  # edges = cv2.drawKeypoints(eqRoi,keys, np.array([]), (0,0,255),cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
  lines = cv2.HoughLinesP(edges,1,np.pi/180, 200, lineSize, 10)
  print(lines)
  cv2.namedWindow('edge', cv2.WINDOW_NORMAL)
  cv2.imshow('edge',edges)
  cv2.waitKey()
  return edges


# known Parameters
areaInMm2 = 156.0
minLineLengthMm = 2.0
#load image
img = cv2.imread('EL_20171221163003_OK.bmp',0)

# Sharpen with Unsharp mask
blur = cv2.GaussianBlur(img, (9,9), 10.0)
img = cv2.addWeighted(img, 1.5, blur, -0.5, 0, img)

threshValue, binIm = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
# img*=binIm
#
kern = np.ones((20,20),np.uint8)
binIm = cv2.morphologyEx(binIm, cv2.MORPH_CLOSE,kern)
binIm = cv2.morphologyEx(binIm, cv2.MORPH_OPEN,kern)
# out,binIm = cv2.threshold(np.uint8(binIm),0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
binIm = cv2.erode(binIm, kern)
# img = img * binIm

numLabels, labels, stats, centroids = cv2.connectedComponentsWithStats(binIm, 8, cv2.CV_32S)
# print(numLabels)
# print(labels)
# print(stats)
binImShow = np.reshape(binIm,(binIm.shape[0],binIm.shape[1],1))
binImShow=np.uint8(np.concatenate((binImShow,binImShow,binImShow),2))

avgSz = 0.0
maxW = 0
maxH = 0
for i in range(numLabels):
  if (stats[i][cv2.CC_STAT_AREA] < binIm.size/8):
    print(stats[i][cv2.CC_STAT_AREA])
    cv2.circle(binImShow, (int(centroids[i][0]),int(centroids[i][1])), 20, (0,255,0),-1)
    avgSz += float(stats[i][cv2.CC_STAT_AREA])
    h = stats[i][cv2.CC_STAT_HEIGHT]
    w = stats[i][cv2.CC_STAT_WIDTH]
    if (h > maxH):
      maxH = h
    if (w > maxW):
      maxW = w

cv2.namedWindow('bin', cv2.WINDOW_NORMAL)
cv2.imshow('bin',binImShow)
avgSz /= numLabels-1
avgSz/=2
print("Average area pix:")
print(avgSz)
print("Pix per mm:")
pixPerMm = math.sqrt(avgSz) / math.sqrt(areaInMm2)
print(pixPerMm)
allRois = np.zeros((maxH,maxW),np.float64)
# img = 255-img
for i in range(numLabels):
  if (stats[i][cv2.CC_STAT_AREA] < binIm.size/8):
    h = stats[i][cv2.CC_STAT_HEIGHT]
    w = stats[i][cv2.CC_STAT_WIDTH]
    roi = img[
        stats[i][cv2.CC_STAT_TOP]:(stats[i][cv2.CC_STAT_TOP]+h),
        stats[i][cv2.CC_STAT_LEFT]:(stats[i][cv2.CC_STAT_LEFT]+w)]
    # output = processRoi(roi, minLineLengthMm * pixPerMm)
    # roi = img[
    #     stats[i][cv2.CC_STAT_TOP]:(stats[i][cv2.CC_STAT_TOP]+stats[i][cv2.CC_STAT_HEIGHT]),
    #     stats[i][cv2.CC_STAT_LEFT]:(stats[i][cv2.CC_STAT_LEFT]+stats[i][cv2.CC_STAT_WIDTH])]
    # stdRoi = np.zeros((maxH,maxW),np.float64)
    # stdRoi[0:stats[i][cv2.CC_STAT_HEIGHT],0:stats[i][cv2.CC_STAT_WIDTH]] = roi
    # stdRoi = roi
    # allRois += stdRoi

# allRois/=numLabels-1
# allRois/=np.max(allRois)
output = processRoi(img, minLineLengthMm * pixPerMm)
for out in
